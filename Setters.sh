#!/bin/bash

# Definición de una variable privada
_nombre="Juan"

# Función para asignar un valor a la variable
set_nombre() {
  nuevo_nombre="$1"
  _nombre="$nuevo_nombre"
}

# Obtener el valor inicial de la variable
echo "Mi nombre es $_nombre"  # Output: Mi nombre es Juan

# Llamar a la función de "setter" para modificar el valor de la variable
set_nombre "Carlos"

# Obtener el valor actualizado de la variable
echo "Mi nombre es $_nombre"  # Output: Mi nombre es Carlos

