#!/bin/bash

echo ""
echo ""
read -p "Ingrese la referencia del producto: " referenciaP
read -p "Ingrese la cantidad: " cantidad

# Getters
get_referencia() {
    echo "$referenciaP"
}

get_cantidad() {
    echo "$cantidad"
}

# Setters
set_referencia(){
    referenciaP="$1"
}

set_cantidad() {
    cantidad="$1"
}

echo ""
echo ""
echo "Referencia: $(get_referencia)"
echo "Cantidad: $(get_cantidad)"
