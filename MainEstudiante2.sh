#!/bin/bash

set_nombres() {
    nombres="$1"
}

set_apellidos() {
    apellidos="$1"
}

set_carnet() {
    carnet="$1"
}

set_carrera() {
    carrera="$1"
}

set_edad() {
    edad="$1"
}

get_nombres() {
   echo "$nombres"
}

get_apellidos() {
    echo "$apellidos"
}

get_carnet() {
    echo "$carnet"
}

get_carrera() {
    echo "$carrera"
}

get_edad() {
    echo "$edad"
}


echo ""

echo "Ingrese los nombres: "
read nombres
set_nombres $nombres
echo "Ingrese los apellidos: "
read apellidos
set_apellidos $apellidos
echo "Ingrese el carnet: "
read carnet
set_carnet $carnet
echo "Ingrese la carrera: "
read carrera
set_carrera $carrera
echo "Ingrese la edad: "
read edad
set_edad $edad

echo ""
echo "Nombres: $(get_nombres)"
echo "Apellidos: $(get_apellidos)"
echo "Carnet: $(get_carnet)"
echo "Carrera: $(get_carrera)"
echo "Edad: $(get_edad)"
