#!/bin/bash

set_referenciaP() {
    ingreso="$1"
}

set_cantidad() {
    cantidad="$1"
}

get_referenciaP() {
   echo "$ingreso"
}

get_cantidad() {
   echo "$cantidad"
}

echo ""

echo "Ingrese la referencia del producto: " 
read ingreso
set_referenciaP $ingreso

echo "Ingrese la cantidad del producto: " 
read cantidad
set_cantidad $cantidad

echo ""
echo "Referencia: $(get_referenciaP)"
echo "Cantidad: $(get_cantidad)"


