#!/bin/bash

echo ""
echo ""
read -p "Ingrese los nombres: " nombres
read -p "Ingrese los apellidos: " apellidos
read -p "Ingrese el carnet: " carnet
read -p "Ingrese la carrera: " carrera
read -p "Ingrese la edad: " edad

# Getters
get_nombres() {
    echo "$nombres"
}

get_apellidos() {
    echo "$apellidos"
}

get_carnet() {
    echo "$carnet"
}

get_carrera() {
    echo "$carrera"
}

get_edad() {
    echo "$edad"
}

# Setters
set_nombres() {
    nombres="$1"
}

set_apellidos() {
    apellidos="$1"
}

set_carnet() {
    carnet="$1"
}

set_carrera() {
    carrera="$1"
}

set_edad() {
    edad="$1"
}


echo ""
echo ""
echo "Nombres: $(get_nombres)"
echo "Apellidos: $(get_apellidos)"
echo "Carnet: $(get_carnet)"
echo "Carrera: $(get_carrera)"
echo "Edad: $(get_edad)"
