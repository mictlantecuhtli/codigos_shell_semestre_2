#!/bin/bash

division() {
    numerador=$1
    denominador=$1
    resultado=$((numerador/denominador))
    echo ""
    echo "Numerador: $numerador"
    echo "Denominador: $denominador"
    echo "Resultado: $resultado"
}

echo "Ingrese el numerador: "
read numerador
echo "Ingrese el denominador: "
read denominador

division $numerador $denominador
