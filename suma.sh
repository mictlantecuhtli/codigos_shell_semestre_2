#!/bin/bash

# Definir una función para sumar dos números
suma() {
  num1=$1 # El primer número se guarda en la variable "num1"
  num2=$2 # El segundo número se guarda en la variable "num2"
  resultado=$((num1 + num2)) # Se suma num1 y num2 y se guarda en "resultado"
  echo "El resultado de la suma es: $resultado"
}

# Pedir al usuario que ingrese los números
echo "Ingrese el primer número: "
read num1
echo "Ingrese el segundo número: "
read num2

# Llamar a la función para sumar los números ingresados
suma $num1 $num2
