#!/bin/bash

attempts=0
max_attempts=3

function login {
    read -p "Username: " username
    read -sp "Password: " password
    echo

    # Verificar el nombre de usuario y la contraseña ingresados aquí
    # ...

    if [[ $username == "admin" && $password == "admin123" ]]; then
        echo "Login successful!"
        exit 0
    else
        attempts=$((attempts + 1))
        echo "Invalid username or password. Attempts left: $((max_attempts - attempts))"

        if [[ $attempts -ge $max_attempts ]]; then
            echo "Maximum login attempts reached. Exiting..."
            exit 1
        fi

        login
    fi
}

echo "Login Form"
echo "----------"
login

