#!/bin/bash

attempts=0
max_attempts=3

function login {
    username=$(zenity --entry --title="Login" --text="Username:")
    password=$(zenity --password --title="Login" --text="Password:")

    # Verificar el nombre de usuario y la contraseña ingresados aquí
    # ...

    if [[ $username == "admin" && $password == "admin123" ]]; then
        zenity --info --title="Login" --text="Login successful!"
        exit 0
    else
        attempts=$((attempts + 1))
        zenity --error --title="Login" --text="Invalid username or password. Attempts left: $((max_attempts - attempts))"

        if [[ $attempts -ge $max_attempts ]]; then
            zenity --error --title="Login" --text="Maximum login attempts reached. Exiting..."
            exit 1
        fi

        login
    fi
}

zenity --info --title="Login" --text="Login Form"
login

